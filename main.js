import { createApp } from 'vue'
import App from './App.vue'
import store from './store/store.js'
import 'material-icons/iconfont/material-icons.css'
import 'materialize-css/dist/css/materialize.min.css'
import 'materialize-css/dist/js/materialize.min.js'
import 'sweetalert2/dist/sweetalert2.min.js'
import 'sweetalert2/dist/sweetalert2.min.css'
import router from './router/router.js'

createApp(App)
.use(router)
.use(store)
.mount('#app')


